<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Category::latest()->get();
        return view('categories.list-category',['data' => $data, 'title' => 'List-Category']);
        // return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|min:3',
        ]);
        $data['slug'] = Str::slug($data['name']);
        Category::create($data);
        return back()->with('Success', 'Category has ben created');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = $request->validate([
            'name' => 'required|min:3',
        ]);
        $data['slug'] = Str::slug($data['name']);
        Category::find($id)->update($data);
        return back()->with('Success', 'Category has ben updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, string $id)
    {
        Category::find($id)->delete();
        return back()->with('Success', 'Category has ben deleted');
    }
}
