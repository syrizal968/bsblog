<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('dashboard',['title'=> 'dashboard']);
});
Route::get('/categories', [CategoryController::class, 'index'])->name('categories');
Route::post('/categories', [CategoryController::class, 'store'])->name('categories');
Route::put('/categories/{id}', [CategoryController::class, 'update'])->name('categories');
Route::delete('/categories/{id}', [CategoryController::class, 'destroy'])->name('categories');

// return Route::resource('/categories', CategoryController::class)->only(
//     'index', 'store', 'update', 'destroy'
// );

Route::get('/post', [PostController::class, 'index'])->name('post');
