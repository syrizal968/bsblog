<nav class="pc-sidebar">
    <div class="navbar-wrapper">
        <div class="m-header">
            <a href="../dashboard/index.html" class="b-brand text-primary">
                <!-- ========   Change your logo from here   ============ -->
                {{-- <img src="{{ asset('images/logo-white.svg') }}" class="img-fluid logo-lg" alt="logo"> --}}
                <img src="{{ asset('images/logo-white.svg') }}" alt="logo">
            </a>
        </div>
        <div class="navbar-content">
            <ul class="pc-navbar">
                <li class="pc-item pc-caption">
                    <label>Navigation</label>
                </li>
                <li class="pc-item">
                    <a href="/" class="pc-link">
                        <span class="pc-micon">
                            <i data-feather="home"></i>
                        </span>
                        <span class="pc-mtext">Dashboard</span>
                    </a>
                </li>
                <li class="pc-item">
                    <a href="{{ url('/post') }}" class="pc-link">
                        <span class="pc-micon">
                            <i data-feather="feather"></i>
                        </span>
                        <span class="pc-mtext">Post</span>
                    </a>
                </li>
                <li class="pc-item">
                    <a href="{{ url('categories') }}" class="pc-link">
                        <span class="pc-micon">
                            <i data-feather="tag"></i>
                        </span>
                        <span class="pc-mtext">Categoris</span>
                    </a>
                </li>
                <li class="pc-item">
                    <a href="../dashboard/index.html" class="pc-link">
                        <span class="pc-micon">
                            <i data-feather="message-square"></i>
                        </span>
                        <span class="pc-mtext">Comments</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
