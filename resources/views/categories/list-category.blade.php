@extends('layout.layout')
@section('conten')
    <main>
        <!-- [ Main Content ] start -->
        <div class="row">
            <!-- [ sample-page ] start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>List Category</h5>
                    </div>
                    <div class="mt-2 ms-3 me-3">
                        <a type="button" class="badge me-2 bg-baru text-white f-12" data-bs-toggle="modal"
                            data-bs-target="#createdcategory"><i data-feather="plus"></i></a>
                        @if ($errors->any())
                            <div class="my-3">
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        @if (session('Success'))
                            <div class="my-3">
                                <div class="alert alert-success">
                                    <ul>
                                        {{ session('Success') }}
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="card-body px-0 py-3">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="unread">
                                        <td class="text-center">#</td>
                                        <td>Name</td>
                                        <td>Slug</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $item)
                                        <tr class="unread">
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->slug }}</td>
                                            <td class="text-center">
                                                <a type="button" class="badge me-2 bg-brand-color-1 text-white f-12 mb-2"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#editcategory{{ $item->id }}"><i
                                                        data-feather="edit"></i></a>
                                                <a type="button" class="badge me-2 bg-googleplus text-white f-12 mb-2"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#deletecategory{{ $item->id }}"><i
                                                        data-feather="trash-2"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ sample-page ] end -->
        </div>
        <!-- [ Main Content ] end -->
        <!-- Modal -->
        <div class="modal fade" id="createdcategory" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-brand-color-1">
                        <h5 class="modal-title" id="staticBackdropLabel">Add Category</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ url('categories') }}" method="post">
                            @csrf
                            <div class="form-group mb-3">
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                    id="floatingInput" placeholder="Name" name="name" value="{{ old('name') }}">
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        @foreach ($data as $item)
            <div class="modal fade" id="editcategory{{ $item->id }}" data-bs-backdrop="static" data-bs-keyboard="false"
                tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-brand-color-1">
                            <h5 class="modal-title" id="staticBackdropLabel">Edit Category</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('categories/' . $item->id) }}" method="post">
                                @method('PUT')
                                @csrf
                                <div class="form-group mb-3">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                        id="floatingInput" placeholder="Name" name="name"
                                        value="{{ old('name', $item->name) }}">
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        @foreach ($data as $item)
            <div class="modal fade" id="deletecategory{{ $item->id }}" data-bs-backdrop="static"
                data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-googleplus">
                            <h5 class="modal-title" id="staticBackdropLabel">Edit Category</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ url('categories/' . $item->id) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <div class="form-group mb-3">
                                    <p>Are you sure want to delete Category with, name <u>{{ $item->name }}</u>...?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-bs-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </main>
    {{-- @include('categories.edit-category') --}}
@endsection
