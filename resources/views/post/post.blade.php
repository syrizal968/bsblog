@extends('layout.layout')
@section('conten')
    <main>
        <div class="row">
            <!-- [ sample-page ] start -->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>List Category</h5>
                    </div>
                    <div class="mt-2 ms-3 me-3">
                        <a type="button" class="badge me-2 bg-baru text-white f-12" data-bs-toggle="modal"
                            data-bs-target="#createdcategory"><i data-feather="plus"></i></a>
                        {{-- @if ($errors->any())
                            <div class="my-3">
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        @if (session('Success'))
                            <div class="my-3">
                                <div class="alert alert-success">
                                    <ul>
                                        {{ session('Success') }}
                                    </ul>
                                </div>
                            </div>
                        @endif --}}
                    </div>
                    <div class="card-body px-0 py-3">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr class="unread">
                                        <td class="text-center">#</td>
                                        <td>Title</td>
                                        <td>Slug</td>
                                        <td>Description</td>
                                        <td>Img</td>
                                        <td>Category</td>
                                        <td>Views</td>
                                        <td>Status</td>
                                        <td>Publish</td>
                                        <td class="text-center">Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($data as $item) --}}
                                    <tr class="unread">
                                        <td class="text-center">1</td>
                                        <td>Title</td>
                                        <td>Slug</td>
                                        <td>Description</td>
                                        <td>Img</td>
                                        <td>Category</td>
                                        <td>Views</td>
                                        <td>Status</td>
                                        <td>Publish</td>
                                        <td class="text-center">
                                            <a type="button" class="badge me-2 bg-brand-color-1 text-white f-12 mb-2">
                                                <i data-feather="edit"></i></a>
                                            <a type="button" class="badge me-2 bg-googleplus text-white f-12 mb-2"><i
                                                    data-feather="trash-2"></i></a>
                                        </td>
                                    </tr>
                                    {{-- @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ sample-page ] end -->
        </div>
    </main>
@endsection
